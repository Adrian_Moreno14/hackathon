import React from 'react'

const Footer = () => {
  return (
        <footer className='bg-dark p-2'>
            <span className='text-light p-2'>Derechos reservados</span>
        </footer>
  )
}

export default Footer