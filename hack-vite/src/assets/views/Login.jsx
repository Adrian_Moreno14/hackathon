import React from 'react'

const Login = () => {
  return (
    <div className='d-flex align-items-center  vh-100'>
      <div id='cont-login' className='container d-flex flex-column align-items-center col-md-4'>
        <i id='use' className="bi bi-people" ></i>
        <div className='form-group mb-2 col-md-8'>
            <input type="text" className='form-control' placeholder='user o email'/>
        </div>
        <div className='form-group col-md-8'>
            <input type="password" name="" id="" className='form-control' placeholder='password'/>
        </div>
        <button type="submit" class="btn btn-primary mt-2 mb-2">Ingresar</button>
      </div>
    </div>
  )
}

export default Login